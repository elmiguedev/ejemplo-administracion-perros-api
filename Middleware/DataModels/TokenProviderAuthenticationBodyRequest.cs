namespace EjemploAdministracionPerrosApi.Middleware.DataModels
{
    public class TokenProviderAuthenticationBodyRequest
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}