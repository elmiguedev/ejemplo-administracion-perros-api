using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using EjemploAdministracionPerrosApi.Middleware.DataModels;
using EjemploAdministracionPerrosApi.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace EjemploAdministracionPerrosApi.Middleware
{
    public class TokenProviderMiddleware
    {
        // Atributos
        // ------------------------------------

        private readonly RequestDelegate _next;
        private readonly TokenProviderOptions _options;
        private static EjemploAdministracionPerrosContext dbcontext = new EjemploAdministracionPerrosContext();


        // Constructor
        // ------------------------------------

        public TokenProviderMiddleware(
            RequestDelegate next,
            IOptions<TokenProviderOptions> options
        )
        {
            _next = next;
            _options = options.Value;
        }

        // Metodos
        // ------------------------------------

        public Task Invoke(HttpContext context)
        {
            if (!context.Request.Path.Equals(_options.Path, StringComparison.Ordinal))
            {
                return _next(context);
            }

            if (!context.Request.Method.Equals("POST"))
            {
                context.Response.StatusCode = 400;
                return context.Response.WriteAsync("Bad Request");
            }

            return GenerateToken(context);
        }

        private async Task GenerateToken(HttpContext context)
        {
            // 1. Obtiene la informacion de autenticacion
            StreamReader reader = new StreamReader(context.Request.Body);
            string jsonContent = reader.ReadToEnd();
            TokenProviderAuthenticationBodyRequest userRequest = JsonConvert
                .DeserializeObject<TokenProviderAuthenticationBodyRequest>(jsonContent);
            
            var username = userRequest.username;
            var password = userRequest.password;

            // 2. Busca en la base de datos y comprueba existencia
            Usuario oUsuario = dbcontext.Usuario.Include("Per").FirstOrDefault(x => x.UsuUsuario == username);

            if (oUsuario == null)
            {
                context.Response.StatusCode = 400;
                await context.Response.WriteAsync("Invalid username or password");
                return;
            }

            // 3. Crea las claims asociadas a la autenticacion del usuario
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(JwtRegisteredClaimNames.Sub, username));
            claims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            claims.Add(new Claim(JwtRegisteredClaimNames.Iat, DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString(), ClaimValueTypes.Integer64));

            // si el usuario puede tener mas de un rol, se recorren todos los
            // roles del usuario y se agrega una claim por cada user, en este
            // caso, como tenemos un solo rol, solo agregamos una claim
            claims.Add(new Claim(ClaimTypes.Role, oUsuario.Per.PerNombre));

            // 4. Crea el token de autenticacion
            var now = DateTime.UtcNow;

            var jwt = new JwtSecurityToken(
                issuer: _options.Issuer,
                audience: _options.Audience,
                claims: claims,
                notBefore: now,
                expires: now.Add(_options.Expiration),
                signingCredentials: _options.SigningCredentials
            );

            // 5. Codifica el token
            var encoded_jwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            // 6. Arma la respuesta para el cliente con el token
            var response = new
            {
                access_token = encoded_jwt,
                expires_in = (int)_options.Expiration.TotalSeconds
            };

            // 7. Envia la respuesta
            context.Response.ContentType = "application/json";
            await context.Response.WriteAsync(JsonConvert.SerializeObject(
                response,
                new JsonSerializerSettings { Formatting = Formatting.Indented }
            ));

        }




    }
}