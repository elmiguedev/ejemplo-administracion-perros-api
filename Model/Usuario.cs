﻿using System;
using System.Collections.Generic;

namespace EjemploAdministracionPerrosApi.Model
{
    public partial class Usuario
    {
        public Usuario()
        {
            PerroUsuIdModificacionNavigation = new HashSet<Perro>();
            PerroUsuIdRegistroNavigation = new HashSet<Perro>();
        }

        public int UsuId { get; set; }
        public string UsuNombre { get; set; }
        public string UsuUsuario { get; set; }
        public int PerId { get; set; }
        public bool UsuActivo { get; set; }

        public virtual ICollection<Perro> PerroUsuIdModificacionNavigation { get; set; }
        public virtual ICollection<Perro> PerroUsuIdRegistroNavigation { get; set; }
        public virtual Perfil Per { get; set; }
    }
}
