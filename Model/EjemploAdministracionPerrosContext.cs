﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace EjemploAdministracionPerrosApi.Model
{
    public partial class EjemploAdministracionPerrosContext : DbContext
    {
        public virtual DbSet<Perfil> Perfil { get; set; }
        public virtual DbSet<Perro> Perro { get; set; }
        public virtual DbSet<Raza> Raza { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            #warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
            optionsBuilder.UseSqlServer(@"Server=localhost;Database=EjemploAdministracionPerros;User Id=sa;Password=clave;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Perfil>(entity =>
            {
                entity.HasKey(e => e.PerId)
                    .HasName("PK_Perfil");

                entity.Property(e => e.PerId)
                    .HasColumnName("perId")
                    .ValueGeneratedNever();

                entity.Property(e => e.PerActivo).HasColumnName("perActivo");

                entity.Property(e => e.PerNombre)
                    .IsRequired()
                    .HasColumnName("perNombre")
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<Perro>(entity =>
            {
                entity.HasKey(e => e.PrrId)
                    .HasName("PK_Perro");

                entity.Property(e => e.PrrId).HasColumnName("prrId");

                entity.Property(e => e.PrrActivo).HasColumnName("prrActivo");

                entity.Property(e => e.PrrFechaModificacion)
                    .HasColumnName("prrFechaModificacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrrFechaRegistro)
                    .HasColumnName("prrFechaRegistro")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrrNombre)
                    .IsRequired()
                    .HasColumnName("prrNombre")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.RazId).HasColumnName("razId");

                entity.Property(e => e.UsuIdModificacion).HasColumnName("usuIdModificacion");

                entity.Property(e => e.UsuIdRegistro).HasColumnName("usuIdRegistro");

                entity.HasOne(d => d.Raz)
                    .WithMany(p => p.Perro)
                    .HasForeignKey(d => d.RazId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Perro_Raza");

                entity.HasOne(d => d.UsuIdModificacionNavigation)
                    .WithMany(p => p.PerroUsuIdModificacionNavigation)
                    .HasForeignKey(d => d.UsuIdModificacion)
                    .HasConstraintName("FK_Table_1_UsuarioModificacion");

                entity.HasOne(d => d.UsuIdRegistroNavigation)
                    .WithMany(p => p.PerroUsuIdRegistroNavigation)
                    .HasForeignKey(d => d.UsuIdRegistro)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Table_1_UsuarioRegistro");
            });

            modelBuilder.Entity<Raza>(entity =>
            {
                entity.HasKey(e => e.RazId)
                    .HasName("PK_Raza");

                entity.Property(e => e.RazId).HasColumnName("razId");

                entity.Property(e => e.RazActivo).HasColumnName("razActivo");

                entity.Property(e => e.RazNombre)
                    .IsRequired()
                    .HasColumnName("razNombre")
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.HasKey(e => e.UsuId)
                    .HasName("PK_Usuario");

                entity.Property(e => e.UsuId).HasColumnName("usuId");

                entity.Property(e => e.PerId).HasColumnName("perId");

                entity.Property(e => e.UsuActivo).HasColumnName("usuActivo");

                entity.Property(e => e.UsuNombre)
                    .IsRequired()
                    .HasColumnName("usuNombre")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.UsuUsuario)
                    .IsRequired()
                    .HasColumnName("usuUsuario")
                    .HasColumnType("varchar(50)");

                entity.HasOne(d => d.Per)
                    .WithMany(p => p.Usuario)
                    .HasForeignKey(d => d.PerId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Usuario_Perfil");
            });
        }
    }
}