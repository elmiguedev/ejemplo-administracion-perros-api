Preparacion del entorno de desarrollo:
--------------------------------------

1) Instalar node:
	https://nodejs.org/es/
	
2) Instalar Net Core:
	https://www.microsoft.com/net/core#windowscmd

3) Instalar Angular Cli:
	https://cli.angular.io/
	
4) Instalar GIT:
	https://git-scm.com/
	https://www.sourcetreeapp.com/ (Opcional)
	
5) Instalar VsCode (Visual Studio Code):
	https://code.visualstudio.com/
	
6) Instalar las siguienes extensiones de VsCode:
	C#
	C# Extensions
	ASP.NET Helper
	Angular v4 TypeScript Snippets
	vscode-icons
	Bootstrap 3 Snippets
    ASPNET Core Snippets